import React, { Component } from 'react';
import './header.scss'
import Logo from './assets/logo.png';
import Icon from './assets/icon.png';


export default class Header extends Component {
    render() {
        return (
            <header>
                <div id="header">
                    <div className="logo-container">
                        <img src={Logo} alt="" />
                        <span>پذیرش 24</span>
                    </div>
                    <div className="menu-container">
                        <ul>
                            <li>برای بیماران</li>
                            <li>برای پزشکان و مراجع درمانی</li>
                            <li>پذیرش 24</li>
                            <li>|</li>
                            <li>
                                <img src={Icon} alt="" />
                                <span>ثبت‌نام / ورود</span>
                            </li>
                        </ul>
                        <div className="login">
                            <img src={Icon} alt="" />
                            <span>ثبت‌نام / ورود</span>
                        </div>
                    </div>
                </div>
            </header>
        )
    }
}
