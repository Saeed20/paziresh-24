import React from 'react';
import './App.scss';
import Header from './Header';
import FirstView from './FirstView';
import Main from './Main';
import Footer from './Footer';

function App() {
  return (
    <div className="App">
      <Header />
      <FirstView />
      <Main />
      <Footer />
    </div>
  );
}

export default App;
