import React, { Component } from 'react'
import './firstview.scss';
import Icon from './assets/icon.png';
import Doctor from './assets/doctor.png';





export default class FirstView extends Component {
    render() {
        return (
            <section className='first-view'>
                <div className="inner-content">
                    <h1>24 ساعته پذیرش شوید!</h1>
                    <div className="search-menu">
                        <input type="text" placeholder='متخصص دندانپزشکی ...' />
                        <div className="filters">
                            <div className="btn-icon">
                                <img src={Icon} alt="" />
                                <span className='title'>فیلترها</span>
                            </div>
                            <div className="btn-icon other-scale">
                                <img src={Icon} alt="" />
                                <span className='title'>استان</span>
                            </div>
                            <div className="btn-icon other-scale">
                                <img src={Icon} alt="" />
                                <span className='title'>نوع مرکز</span>
                            </div>
                            <div className="btn-icon other-scale">
                                <img src={Icon} alt="" />
                                <span className='title'>نوع خدمت</span>
                            </div>
                            <div className="btn-icon other-scale">
                                <img src={Icon} alt="" />
                                <span className='title'>همه تخصص‌ها</span>
                            </div>
                        </div>
                    </div>
                </div>
                <img src={Doctor} alt="" id='bg' />

            </section>
        )
    }
}
