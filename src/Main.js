import React, { Component } from 'react';
import './main.scss';
import Logo from './assets/logo.png';


export default class Main extends Component {
    render() {
        return (
            <main>
                <div className="main-side">
                    <div className="breadcrumbs">
                        <ul>
                            <li>خانه</li>
                            <li>/</li>
                            <li>جستجو</li>
                        </ul>
                    </div>
                    {/* card-1 */}
                    <div className="card">
                        <div className="content">
                            <img src={Logo} alt="LOGO" />
                            <div className="corner-badg">
                                <span><span className='star'>&#8902;</span><span>4.5</span></span>
                            </div>
                            <div className="txt">
                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}><span className='suggest'>پیشنهاد</span> <div className="verify-badg"></div><h2>مرکز مشاوره آنلاین</h2></div>
                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}><span className="verify-badg"></span><span className='number'>100 </span> <span className='title'>پزشک آنلاین</span></div>
                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}><span className="verify-badg"></span><span className='number'>98% </span> <span className='title'>رضایت</span></div>
                            </div>
                        </div>
                        <div className="line">
                            <div className="container">
                                <div className="icon"></div>
                            </div>
                        </div>
                        <div className="btn-container">
                            {/* <p className='btn-context'>عنوان</p> */}
                            <div className="btn btn-darkcyan">مشاهده پزشکان و دریافت مشاوره</div>
                        </div>
                    </div>
                    {/* card-2 */}
                    <div className="card">
                        <div className="content">
                            <div className="corner-badg">
                            <img src={Logo} alt="LOGO" />
                                <span><span className='star'>&#8902;</span><span>4.5</span></span>
                                <div>(5 نفر)</div>
                            </div>
                            <div className="txt">
                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}><span className='suggest'>پیشنهاد</span> <div className="verify-badg"></div><h2>دکتر مهدی صائم</h2></div>
                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}><span className='title'>چشم پزشکی</span></div>
                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}><span className='title'>بیمارستان فلاحی | تهران، تهران</span></div>
                            </div>
                        </div>
                        <div className="line">
                            <div className="container">
                                <div className="icon"></div>
                            </div>
                        </div>
                        <div className="multi-btn-container">
                            <div className="btn-container">
                                <p className='btn-context btn-context-green'>اولین نوبت خالی: 18 ابان</p>
                                <div className="btn btn-green">رزرو نوبت حضوری</div>
                            </div>
                            <div className="spacer"></div>
                            <div className="btn-container">
                                <p className='btn-context btn-context-darkcyan'>زودترین زمان مشاوره: 18 آبان</p>
                                <div className="btn btn-darkcyan">دریافت مشاوره</div>
                            </div>
                        </div>
                    </div>

                    {/* card-3 */}
                    <div className="card">
                        <div className="content">
                            <img src={Logo} alt="LOGO" />
                            <div className="corner-badg">
                                <span><span className='star'>&#8902;</span><span>4.5</span></span>
                            </div>
                            <div className="txt">
                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}><h2>دکتر مهدی صائم</h2></div>
                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}><span className='title'>چشم پزشکی</span></div>
                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}><span className='title'>بیمارستان فلاحی | تهران، تهران</span></div>
                            </div>
                        </div>
                        <div className="line">
                            <div className="container">
                                <div className="icon"></div>
                            </div>
                        </div>
                        <div className="btn-container">
                            {/* <p className='btn-context'>عنوان</p> */}
                            <div className="btn btn-darkcyan-outline">مشاهده پروفایل</div>
                        </div>
                    </div>
                    {/* card-4 */}
                    <div className="card">
                        <div className="content">
                            <img src={Logo} alt="LOGO" />
                            <div className="corner-badg">
                                <span><span className='star'>&#8902;</span><span>4.5</span></span>
                            </div>
                            <div className="txt">
                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}><span className='suggest'>پیشنهاد</span> <div className="verify-badg"></div><h2>بیمارستان الزهرا تهران</h2></div>
                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}><span className='title'>تهران، تهران</span></div>
                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}><span className='title'>خیابان شریعتی، کوچه شهید رستگار، ....</span></div>
                            </div>
                        </div>
                        <div className="line">
                            <div className="container">
                                <div className="icon"></div>
                            </div>
                        </div>
                        <div className="btn-container">
                            {/* <p className='btn-context'>عنوان</p> */}
                            <div className="btn btn-green">مشاهده مرکز و رزرو نوبت</div>
                        </div>
                    </div>


                    {/* card-5 */}
                    <div className="card">
                        <div className="content">
                            <img src={Logo} alt="LOGO" />
                            <div className="corner-badg">
                                <span><span className='star'>&#8902;</span><span>4.5</span></span>
                            </div>
                            <div className="txt">
                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}><span className='suggest'>پیشنهاد</span> <div className="verify-badg"></div><h2>دکتر مهدی صائم</h2></div>
                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}><span className='title'>چضم پزشکی</span></div>
                                <div style={{ display: 'flex', flexDirection: 'row', alignItems: 'center' }}><span className='title'>خیابان شریعتی، کوچه شهید رستگار، ....</span></div>
                            </div>
                        </div>
                        <div className="line">
                            <div className="container">
                                <div className="icon"></div>
                            </div>
                        </div>
                        <div className="btn-container">
                            <p className='btn-context'>نوبت‌دهی غیر فعال</p>
                            <div className="btn btn-darkcyan-outline">مشاهده پزشکان و دریافت مشاوره</div>
                        </div>
                    </div>



                </div>
                <div className="side-bar">
                    <div id="ads"></div>
                    <div id="map"></div>
                </div>
            </main>
        )
    }
}
